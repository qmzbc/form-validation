//用户名：userName
//密码：userPassWord
//手机号：userPhone
//验证码：userCAPTCHA
//获取验证码：btn-CAPTCHA
//注册：btn-registered

window.onload = function () {
  // 获取元素
  let userName = document.getElementById('userName')
  let userPassWord = document.getElementById('userPassWord')
  let userPhone = document.getElementById('userPhone')
  let CAPTCHA = document.getElementById('userCAPTCHA')
  let btnCAPTCHA = document.getElementById('btn-CAPTCHA')
  let btnRegistered = document.getElementById('btn-registered')

  let timer = null

  function isUserName(str) {
    // 判断是否是由4-10位数字,字母,_组成
    let reg = /^\w{4,10}$/
    if (reg.test(str)) {
      userName.classList.remove('is-invalid')
      userName.classList.add('is-valid')
      return true
    } else {
      userName.classList.remove('is-valid')
      userName.classList.add('is-invalid')
      // alert('请创建4-9位的用户名，可以使用数字、字母和"_"')
      return false
    }
  }

  function isUserPassWord(str) {
    // 判断是否是由6-18位数字,大小写字母,"  ~ @ # _ ^ * % / . + : ; =  "组成
    let reg = /^[\w~@#_^*%/.+:;=]{6,18}$/
    if (reg.test(str)) {
      userPassWord.classList.remove('is-invalid')
      userPassWord.classList.add('is-valid')
      return true
    } else {
      userPassWord.classList.remove('is-valid')
      userPassWord.classList.add('is-invalid')
      // alert('请创建6-18位的密码，可以使用数字、大小写字母和"  ~ @ # _ ^ * % / . + : ; =  "')
      return false
    }
  }

  function isUserPhone(str) {
    // 判断是否是正确手机号
    let reg = /^1[3|4|5|7|8][0-9]{9}$/
    if (reg.test(str)) {
      userPhone.classList.remove('is-invalid')
      userPhone.classList.add('is-valid')
      return true
    } else {
      userPhone.classList.remove('is-valid')
      userPhone.classList.add('is-invalid')
      // alert('请输入正确手机号')
      return false
    }
  }
  
  function isCAPTCHA(str) {
    // 判断是否是四位数字
    // 暂时没有学习后端，无法判断是否是正确验证码，暂时以四位数字为标准
    let reg = /^\w{4}$/
    if (reg.test(str)) {
      CAPTCHA.classList.remove('is-invalid')
      CAPTCHA.classList.add('is-valid')
      return true
    } else {
      CAPTCHA.classList.remove('is-valid')
      CAPTCHA.classList.add('is-invalid')
      // alert('请输入正确验证码')
      return false
    }
  }

    // 获取验证码：
    btnCAPTCHA.onclick = function () {
      if(!isUserPhone(userPhone.value)){
        return false
      }
      let time = 60
      btnCAPTCHA.disabled = true
      btnCAPTCHA.classList.remove('btn-success','ml-3')
      btnCAPTCHA.classList.add('btn-secondary','ml-1')
      btnCAPTCHA.innerHTML = time + 's后重新发送'
  
      timer = setInterval(function () {
        if (time <= 1) {
          btnCAPTCHA.classList.remove('btn-secondary','ml-1')
          btnCAPTCHA.classList.add('btn-success','ml-3')
          btnCAPTCHA.innerHTML = '获取验证码'
          btnCAPTCHA.disabled = false
          clearInterval(timer)
        } else {
          time--
          btnCAPTCHA.innerHTML = time + 's后重新发送'
        }
      }, 1000)
    }

  // 提交数据  
  btnRegistered.onclick = function () {
    if(isUserName(userName.value) && isUserPassWord(userPassWord.value) && isUserPhone(userPhone.value) && isCAPTCHA(CAPTCHA.value) == true){
      alert('注册完成~~正在跳转')
      return true
    } else {
      return false
    }
  }
}